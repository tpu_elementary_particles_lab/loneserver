#!/bin/sh

FRESNEL_ADDR=fresnel.qcrypt.org
declare -a KNOCKING_PORTS=("256" "312" "25128")

for p in "${KNOCKING_PORTS[@]}" ; do
    nmap $FRESNEL_ADRR -sS -Pn -p $p
done
